<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digiwords'][$id]['reponse'])) {
		$reponse = $_SESSION['digiwords'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digiwords_nuages WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($nuage = $stmt->fetchAll()) {
			$admin = false;
			if (count($nuage, COUNT_NORMAL) > 0 && $nuage[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $nuage[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digiwords'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digiwords'][$id]['digidrive'];
			}
			echo json_encode(array('nom' => $nuage[0]['nom'], 'donnees' => $donnees, 'admin' =>  $admin, 'digidrive' => $digidrive));
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
