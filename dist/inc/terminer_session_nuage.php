<?php

session_start();

require 'headers.php';

if (!empty($_POST['nuage'])) {
	$nuage = $_POST['nuage'];
	unset($_SESSION['digiwords'][$nuage]['reponse']);
	echo 'session_terminee';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
